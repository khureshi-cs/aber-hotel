﻿<%@ Page Title="Narcissus Tower Riyadh" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="narcisuss-tower-riyadh.aspx.cs" Inherits="aber.narcisuss_tower_riyadh" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
		<h1><span>News</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">Home</a></li>
            <li><a href="news.aspx">News</a></li>            
        </ol>
	</div>
	<!-- End of Internal Page Header -->
	
	<!-- Main Container -->
	<div class="main-content container">
		<!-- Page Content -->
		<div class="page-content col-md-12">			
			<!-- Post Container -->
			<div class="post-container">
				<!-- Post boxes -->
				<div class="post-box">
					<img src="assets/img/news/narcisuss-tower1.jpg" class="post-img" alt="Narcissus Tower Riyadh"/>
					<h3 class="post-title">Narcissus Tower Riyadh</h3>
					<div class="post-desc">
						<p>
							We are pleased to announce very soon the official opening of the Narcissus Tower, adjacent to the Narcissus Hotel Riyadh on Tahlia Street. Narcissus has become a very common name in the world of hospitality, and for the past five years since its opening, it has proudly won four awards for international luxury. Hotel Narcissus is characterized by elegance and beauty in terms of exterior design, which inspires heritage and originality, in addition to the luxury of furnishings and facilities that dazzle the eyes of all who see it, in addition to many other services such as restaurants, gymnasiums, wedding halls, conferences halls, swimming pools and spa in the hotel and many other amenities.
						</p>						
					</div>
					<div class="post-meta clearfix">
						<div class="post-date">28 - Oct - 2017</div>
					</div>				
				</div>
			</div>
			<!-- End of Post Container -->
		</div>		
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
