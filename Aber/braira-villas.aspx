﻿<%@ Page Title="Braira Villas Grand Opening" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="braira-villas.aspx.cs" Inherits="aber.braira_villas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
		<h1><span>News</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">Home</a></li>
            <li><a href="news.aspx">News</a></li>            
        </ol>
	</div>
	<!-- End of Internal Page Header -->
	
	<!-- Main Container -->
	<div class="main-content container">
		<!-- Page Content -->
		<div class="page-content col-md-12">			
			<!-- Post Container -->
			<div class="post-container">
				<!-- Post boxes -->
				<div class="post-box">
					<img src="assets/img/news/braira-hitin.jpg" class="post-img" alt="Braira Villas Grand Opening"/>
					<h3 class="post-title">Braira Villas Grand Opening </h3>
					<div class="post-desc">
						<p>
							Braira Hotel &amp; Villas Will be opened soon to public in Hitin district- Riyadh City. This is the first time in the Kingdom that a hotel &amp; Villa has been established, consisting of private villas comprising all services in an elegant style, furniture that is very luxurious and elegant. The resort is fully integrated in terms of services and entertainment. Each has a private swimming pool and glamour decorations.
						</p>						
					</div>
					<div class="post-meta clearfix">
						<div class="post-date">28 - Oct - 2017</div>
					</div>				
				</div>
			</div>
			<!-- End of Post Container -->
		</div>		
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
