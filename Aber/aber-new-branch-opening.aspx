﻿<%@ Page Title="Aber Hotel " Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="aber-new-branch-opening.aspx.cs" Inherits="aber.aber_new_branch_opening" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
		<h1><span>News</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">Home</a></li>
            <li><a href="news.aspx">News</a></li>            
        </ol>
	</div>
	<!-- End of Internal Page Header -->
	
	<!-- Main Container -->
	<div class="main-content container">
		<!-- Page Content -->
		<div class="page-content col-md-12">			
			<!-- Post Container -->
			<div class="post-container">
				<!-- Post boxes -->
				<div class="post-box">
					<img src="assets/img/news/aber-2.jpg" class="post-img" alt="Aber Hotels"/>
					<h3 class="post-title">Aber Hotel </h3>
					<div class="post-desc">
						<p>
							The new branch of Aber hotels  in ( Sahafa district ) is coming soon to public. The hotels of Aber Brand are distinguished and simple in terms of design, concept and economical prices while ensuring high quality and professionalism in providing services to our guests. These concepts  are became very important  for most of travelers and for who are searching for new lifestyles. This series of hotels offers distinctive services at economical prices. The designs combine modernity with Arabic originality.
						</p>						
					</div>
					<div class="post-meta clearfix">
						<div class="post-date">28 - Oct - 2017</div>
					</div>				
				</div>
			</div>
			<!-- End of Post Container -->
		</div>		
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
