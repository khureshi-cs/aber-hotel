﻿<%@ Page Title="Contact Aber Hotels | Best Luxury Hotels in Saudi Arabia" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="contact-us.aspx.cs" Inherits="aber.contact_us" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="map">
		
	</div>	
	<!-- Contact Page Container -->
	<div class="contact-page-container container">

		<!-- Contact Info -->
		<div class="contact-info-main-box container">
			<div class="contact-info-inner clearfix">
				<div class="contact-info-box col-md-4">
					<i class="fa fa-envelope-o"></i><div class="info">aber101@aberhotels.com</div>
				</div>
				<div class="contact-info-box col-md-4">
					<i class="fa fa-phone"></i><div class="info">011 211 4980</div>
				</div>
				<div class="contact-info-box col-md-4">
					<i class="fa fa-map-marker "></i><div class="info">Al Yasmin, Riyadh 13322 2492, Saudi Arabia</div>
				</div>
			</div>			
		</div>

		<!-- Contact Form -->
		<div class="contact-form-container">
			<div class="how-contact col-md-4">
				<div class="title">We'd love to hear from you!</div>
				<div class="desc">
					Aber Hotels is committed to providing top quality service and endeavors to achieve total guest satisfaction.
				</div>
			</div>
			<div class="contact-form-box col-md-8">
				<div class="contact-form clearfix">
					<div class="col-md-6">
						<input type="text" name="name" placeholder="Full Name :">
						<input type="email" name="email" placeholder="Email :">
						<input type="text" name="phone" placeholder="Phone :">
					</div>
					<div class="col-md-6">
						<textarea name="message" placeholder="Your Message : "></textarea>
					</div>
					<div class="submit-container">
						<input type="submit" value="Submit" class="btn btn-default">
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- End of Contact Page Container -->
    
</asp:Content>
<asp:Content ID="FooterScripts" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages sticky trans-header contact type-2")
    </script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDBdRx3UKvK7wCC1xx-pYVt_Wihfa41S1M"></script>
    <script type="text/javascript">
		"use strict";
        function initialize() {
            var myLatLng = new google.maps.LatLng(24.820327, 46.647978);
            var mapOptions = {
                zoom: 12,
                center: myLatLng,
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{featureType:"landscape",stylers:[{saturation:-100},{lightness:65},{visibility:"on"}]},{featureType:"poi",stylers:[{saturation:-100},{lightness:51},{visibility:"simplified"}]},{featureType:"road.highway",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"road.arterial",stylers:[{saturation:-100},{lightness:30},{visibility:"on"}]},{featureType:"road.local",stylers:[{saturation:-100},{lightness:40},{visibility:"on"}]},{featureType:"transit",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"administrative.province",stylers:[{visibility:"off"}]},{featureType:"administrative.locality",stylers:[{visibility:"off"}]},{featureType:"administrative.neighborhood",stylers:[{visibility:"on"}]},{featureType:"water",elementType:"labels",stylers:[{visibility:"off"},{lightness:-25},{saturation:-100}]},{featureType:"water",elementType:"geometry",stylers:[{hue:"#ffff00"},{lightness:-25},{saturation:-97}]}],

                // Extra options
                scrollwheel: false,
                mapTypeControl: false,
                panControl: false,
                zoomControlOptions: {
                    style   : google.maps.ZoomControlStyle.SMALL,
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                }
            }
            var map = new google.maps.Map(document.getElementById('map'),mapOptions);

            var image = 'assets/img/marker.png';

            var beachMarker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</asp:Content>
