﻿<%@ Page Title="Aber Hotels Saudi Arabia" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="aber._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Top Slider and Booking form -->
    <div id="home-top-section">
        <!-- Main Slider -->
        <div id="main-slider">
            <div class="items">
                <img src="assets/img/slider/aber-banner-6.jpg" alt="aber banner 2" />
            </div>
            <div class="items">
                <img src="assets/img/slider/aber-banner-5.jpg" alt="aber banner 2" />
            </div>
            <div class="items">
                <img src="assets/img/slider/aber-banner-4.jpg" alt="aber banner 2" />
            </div>
            <div class="items">
                <img src="assets/img/slider/aber-banner-3.jpg" alt="aber banner 3" />
            </div>
            <div class="items">
                <img src="assets/img/slider/aber-banner-7.jpg" alt="aber banner 3" />
            </div>
        </div>
        <!-- Booking Form -->
        <div class="booking-form-container container">
            <div id="main-booking-form" class="style-2">
                <h2>Find A <span>Hotel</span></h2>
                <div class="booking-form clearfix">
                    <!-- Do Not remove the classes 
                    <div class="input-daterange clearfix">
                        <div class="booking-fields col-xs-6 col-md-12">
                            <input placeholder="Check in" class="datepicker-fields check-in" type="text" name="start" required />
                            <i class="fa fa-calendar"></i>                            
                        </div>
                        <div class="booking-fields col-xs-6 col-md-12">
                            <input placeholder="Check out" class="datepicker-fields check-out" type="text" name="end" required />
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>-->
                    <div class="booking-fields col-xs-12">
                        <select class="chosen-select" name="city" id="ddlCity" required>
                            <option value="">Select City</option>
                            <optgroup label="Saudi Arabia">
                                <option>Riyadh</option>                                
                            </optgroup>                            
                        </select>
                    </div>
                    <div class="booking-fields col-xs-12">
                        <!-- Select boxes ( you can change the items and its value based on your project's needs ) -->
                        <select name="hotels" id="ddlHotels" data-placeholder="Select Destination" required>
                            <option value=""></option>
                            <!-- Select box items ( you can change the items and its value based on your project's needs ) -->
                        </select>
                        <!-- End of Select boxes -->
                    </div>
                    <!-- <div class="booking-fields col-xs-6 col-md-12 clearfix">                        
                        <select name="room-type" required>
                            <option value="">Adults</option>                           
                            <option value="2">1</option>
                            <option value="3">2</option>
                            <option value="4">3</option>
                            <option value="5">4</option>
                            <option value="6">5</option>
                        </select>                        
                    </div>
                    <div class="booking-fields col-xs-6 col-md-12">
                        <select name="guest">
                            <option value="">Children</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div> -->
                    <div id="lbValidate"></div>
                    <div class="booking-button-container">
                        <a id="lnkBooking" target="_blank" onclick="validateDestination()" class="btn btn-default">Book Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Top Slider and Booking form -->

    <!-- Luxury Rooms -->
    <div id="luxury-rooms">
        <!-- Heading box -->
        <div class="heading-box">
            <h2>About <span>Aber Hotel</span></h2>
        </div>
        <!-- Room Box Container -->
        <div class="room-container container">
            <!-- Room box -->
            <div class="room-boxes">
                <img src="assets/img/rooms/rooms-2.jpg" alt="King Suit" class="room-img"><!-- Room Image -->
                <div class="room-details col-xs-6 col-md-4">
                    <!--Room title <div class="title">King Suit</div>-->
                    <div class="description">
                        <strong>“Boudl Group”</strong> for hotels and resort established the new chain of <strong>“Aber Hotels”</strong> that has been launched in 2016 to meet the needs of several segmentations of our valued 
                        guest, as we present a modern hotels throughout best price concept with the quality which we are always promise to guarantee to our valued guest. The budget concept
                    </div>
                    <a href="about-us.aspx" class="btn btn-default">Details</a><!-- Detail link -->
                </div>
            </div>
        </div>
    </div>
    <!-- End of Luxury Rooms -->

    <!-- Special Packages -->
    <div id="special-packages" class="container">
        <!-- Heading box -->
        <div class="heading-box">
            <h2>Our <span>Brands</span></h2>
        </div>
        <!-- Package Container -->
        <div class="package-container clearfix">
            <!-- Package Box -->
            <div class="col-md-12 wow fadeInUp">
                <ul class="horizontal-images">
                    <li>
                        <a href="http://boudlgroup.com" target="_blank">
                            <img src="assets/img/aber-boudl-group-1.png" alt="Boudl Group" class="img-horizontal" />
                        </a>
                    </li>
                    <li>
                        <a href="http://boudl.com" target="_blank">
                            <img src="assets/img/aber-boudl.png" alt="Aber Hotels" class="img-horizontal" />
                        </a>
                    </li>
                    <li>
                        <a href="http://brairahotels.com" target="_blank">
                            <img src="assets/img/aber-braira.png" alt="Braira Hotels" class="img-horizontal" />
                        </a>
                    </li>
                    <li>
                        <a href="http://www.narcissusriyadh.com/en/" target="_blank">
                            <img src="assets/img/aber-narcissus.png" alt="Boudl Hotels" class="img-horizontal" />
                        </a>
                    </li>
                    <li>
                        <a href="http://aberhotels.com" target="_blank">
                            <img src="assets/img/aber-logo.png" alt="Aber Hotels" class="img-horizontal aber-logo" />
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" target="_blank">
                            <img src="assets/img/bur.png" alt="Bur Bakery" class="img-horizontal aber-logo" />
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" target="_blank">
                            <img src="../assets/img/pampa-grill-logo.png" alt="Pampa Grill" class="img-horizontal pampa-logo" />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End of Special Packages -->

</asp:Content>
<asp:Content ContentPlaceHolderID="ScriptContainer" ID="FooterScripts" runat="server">    
    <script type="text/javascript">
        function validateDestination() {
            var theValue = $("#ddlCity").val();
            if (theValue == "") {
                $("#lbValidate").html("Please select destination");
                $("#lbValidate").attr('style', 'color: Red !important');
            }
            else {
                $("#lbValidate").html("");
            }
        }
        $("body").addClass("homepage trans-header sticky white-datepicker");
        $(document).ready(function () {
            //$("#ddlHotels").chosen("destroy");
            $("#ddlCity").change(function () {
                var strCity = $(this).val();
                $("#ddlHotels").empty();
                $("#ddlHotels").append($('<option>', {
                    value: 0
                }));
                switch (strCity) {
                    case "Riyadh":
                        $('#ddlHotels').append($('<option>', {
                            value: 1,
                            text: 'Aber 101'
                        })).append($('<option>', {
                            value: 2,
                            text: 'Aber 102'
                        }));
                        break;                    
                    default:
                        break;
                }
                $('#ddlHotels').trigger("chosen:updated");
            });

            $("#ddlHotels").change(function () {
                var iHotelType = $(this).val();
                switch (iHotelType) {
                    case "1":
                        $("#lnkBooking").attr("href", "http://aber101.boudl.com/V8Client/Inquiry.aspx");
                        break;
                    case "2":
                        $("#lnkBooking").attr("href", "http://aber102.boudl.com/V8Client/Inquiry.aspx");
                        break;                    
                    default:
                        $("#lnkBooking").attr("href", "");
                        break;
                }
            });
        });
    </script>
</asp:Content>
