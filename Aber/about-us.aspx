﻿<%@ Page Title="About Aber Hotels | Luxury Hotels in Saudi Arabia" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="about-us.aspx.cs" Inherits="aber.about_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Internal Page Header -->
    <div class="internal-page-title about-page" data-parallax="scroll" data-image-src="assets/img/internal-header.jpg">
        <h1>About <span>Aber Hotels</span></h1>
        <ol class="breadcrumb">
            <!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">Home</a></li>
            <li class="active">About</li>
        </ol>
    </div>
    <!-- End of Internal Page Header -->

    <!-- Welcome -->
    <div id="welcome" class="container">
        <!-- Heading box -->
        <div class="heading-box">
            <h2>Welcome to <span>Aber Hotels</span></h2>
            <!-- Title -->
        </div>
        <!-- Inner section -->
        <div class="inner-content">
            <div class="img-frame">
                <img src="assets/img/aber-about-us.jpg" alt="Welcome to Aber">
            </div>
            <div class="desc">
                <p>
                    “Boudl Group” for hotels and resort established the new chain of “Aber Hotels” that has been launched in 2016 to meet the needs of several segmentations of 
                our valued guest, as we present a modern hotels throughout best price concept with the quality which we are always promise to guarantee to our valued guest.
                </p>
                <p>
                    The budget concept becomes nowadays highly required for travelers and whom seeking for break to change the regular live style. 
                    The hotel chain is unique in providing special services with a budget rates and characterized by the modern and Arab genuineness designs. 
                    The Number of “Aber Hotels” has reached 6 hotels located in Riyadh, Qassim, Southern and eastern province and our strategic plan include to spread out 
                    in entire the kingdom regions.
                </p>
            </div>
        </div>
    </div>
    <!-- End of Welcome -->

    <!-- Our Services -->
    <div id="our-services">
        <!-- Heading box -->
        <div class="heading-box">
            <h2>Our <span>Services</span></h2>
            <!-- Title -->
        </div>

        <!-- Service Slider -->
        <div id="services-box" class="owl-carousel owl-theme">
            <div class="item">
                <img src="assets/img/services/swimming-pools.jpg" alt="Swimming Pools">
                <div class="title">Swimming Pools</div>                
            </div>
            <div class="item">
                <img src="assets/img/services/high-speed-internet.jpg" alt="High Speed Internet">
                <div class="title">High Speed Internet</div>
                
            </div>
            <div class="item">
                <img src="assets/img/services/meeting-rooms.jpg" alt="meeting rooms">
                <div class="title">Meeting Rooms</div>                
            </div>
            <div class="item">
                <img src="assets/img/services/relaxing-atmosphere.jpg" alt="Relaxing Atmosphere">
                <div class="title">Relaxing Atmosphere</div>                
            </div>            
        </div>

    </div>
    <!-- End of Our Services -->
</asp:Content>
<asp:Content ID="FooterScripts" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky")
    </script>
</asp:Content>
