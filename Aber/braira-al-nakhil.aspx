﻿<%@ Page Title="Braira Al Nakhil (Riyadh)" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="braira-al-nakhil.aspx.cs" Inherits="aber.braira_al_nakhil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
		<h1><span>News</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">Home</a></li>
            <li><a href="news.aspx">News</a></li>            
        </ol>
	</div>
	<!-- End of Internal Page Header -->
	
	<!-- Main Container -->
	<div class="main-content container">
		<!-- Page Content -->
		<div class="page-content col-md-12">			
			<!-- Post Container -->
			<div class="post-container">
				<!-- Post boxes -->
				<div class="post-box">
					<img src="assets/img/news/al-nakhil.jpg" class="post-img" alt="Braira Al Nakhil ( Riyadh )"/>
					<h3 class="post-title">Braira Al Nakhil (Riyadh)</h3>
					<div class="post-desc">
						<p>
							Braira Hotel &amp; Resort will soon be opening in Al-Nakheel District in Riyadh. The most advantage of the hotels in Braira is the modern decoration, which tends to luxury and simplicity at the same time. The company has established many hotels and resorts throughout the Kingdom and is expanding to several major cities. Braira is the perfect place to spend a holiday with the family, where the guest will enjoy peace and luxury as well as complementary hotel services for unbeatable prices.
						</p>						
					</div>
					<div class="post-meta clearfix">
						<div class="post-date">28 - Oct - 2017</div>
					</div>				
				</div>
			</div>
			<!-- End of Post Container -->
		</div>		
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
