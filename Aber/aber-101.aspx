﻿<%@ Page Title="Aber Hotel Branch 101 Riyadh Saudi Arabia" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="aber-101.aspx.cs" Inherits="aber.aber_101" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="room-detail-page">

        <!-- Main Slider -->
        <div id="room-details-slider">
            <div class="items">
                <img src="http://www.brairahotels.com/assets/img/branches/azizya8.jpg" alt="azizya 1" />
            </div>
            <div class="items">
                <img src="http://www.brairahotels.com/assets/img/branches/azizya2.jpg" alt="azizya 2" />
            </div>
            <div class="items">
                <img src="http://www.brairahotels.com/assets/img/branches/azizya3.jpg" alt="azizya 3" />
            </div>
            <div class="items">
                <img src="http://www.brairahotels.com/assets/img/branches/azizya4.jpg" alt="azizya 4" />
            </div>
            <div class="items">
                <img src="http://www.brairahotels.com/assets/img/branches/azizya5.jpg" alt="azizya 5" />
            </div>
        </div>

        <div class="booking-title-box">
            <div class="booking-title-box-inner container">
                <!-- Heading box -->
                <div class="heading-box">
                    <h2><span>Aber</span> 101</h2>
                    <!-- Title -->
                    <div class="subtitle price"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><span>- Three Star Hotels</span></div>
                    <!-- Subtitle -->
                </div>

                <!-- Booking form -->
                <div class="booking-form clearfix">
                    <!-- Do Not remove the classes -->
                    <div class="input-daterange col-md-6">
                        <div class="booking-fields col-md-6">
                            <input placeholder="Check-in" class="datepicker-fields check-in" type="text" name="start" autocomplete="off" /><!-- Date Picker field ( Do Not remove the "datepicker-fields" class ) -->
                            <i class="fa fa-calendar"></i>
                            <!-- Date Picker Icon -->
                        </div>
                        <div class="booking-fields col-md-6">
                            <input placeholder="Check-Out" class="datepicker-fields check-out" type="text" name="end" autocomplete="off" />
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                    <div class="booking-fields col-md-3">
                        <!-- Select boxes ( you can change the items and its value based on your project's needs ) -->
                        <select name="room-type" id="booking-field2">
                            <option value="">Adult</option>
                            <!-- Select box items ( you can change the items and its value based on your project's needs ) -->
                            <option value="2">1</option>
                            <option value="3">2</option>
                            <option value="4">3</option>
                            <option value="5">4</option>
                            <option value="6">5</option>
                        </select>
                        <!-- End of Select boxes -->
                    </div>
                    <div class="booking-fields col-md-3">
                        <select name="guest" id="booking-field3">
                            <option value="1">Children</option>
                            <option value="2">1</option>
                            <option value="3">2</option>
                            <option value="4">3</option>
                            <option value="5">4</option>
                            <option value="6">5</option>
                        </select>
                    </div>
                    <div class="booking-button-container">
                        <input class="btn btn-default" value="Book Now" type="submit" onclick="window.open('http://azizia.boudl.com/V8Client/Inquiry.aspx');" /><!-- Submit button -->
                    </div>
                </div>
            </div>
        </div>
        <div class="room-details container">
            <div class="description">
                <p>
                    Located in Al Khobar in the region of Ash-Sharqīyah‎, 3.2 km from Sunset Marina, Braira AL Azizya Hotel &amp; Resort features a private beach area and water sports facilities. Free private parking is available on site.
                </p>
                <p>
                    Every room at this resort is air conditioned and is fitted with a flat-screen TV with satellite channels. Some rooms feature a sitting area where you can relax. Enjoy a cup of coffee or tea while looking out at the garden or city. Every room is fitted with a private bathroom. For your comfort, you will find bathrobes and slippers.
                </p>
                <p>
                    There is a 24-hour front desk at the property.<br />
                    Half Moon Bay is 12.9 km from Braira AL Azizya Hotel &amp; Resort, and Al Rashid Mall is 16.1 km away. King Fahd Airport is 51.5 km from the property.

                </p>
                <br />
                <h1>* Check-in: 12 p.m. * Check-out: 2 a.m.
                </h1>
            </div>
        </div>
    </div>
    <!-- Special Packages -->
    <div id="special-packages-type-2" class="container">
        <!-- Room Box Container -->
        <div class="package-container">
            <!-- package box -->
            <div class="package-boxes wow fade fadeInUp">
                <img src="http://www.brairahotels.com/assets/img/branches/azizya-features.jpg" alt="Braira Azizya Features" class="package-img" />
                <div class="package-details col-md-6 col-lg-4">
                    <div class="title">Most Popular Facilities</div>
                    <!-- package title -->
                    <div class="description">
                        <!-- package Description -->
                        <ul class="list-inline">
                            <li><i class="fa fa-building"></i>50 Rooms</li>
                            <li><i class="fa fa-building-o"></i>Beach-front</li>
                            <li><i class="fa fa-bed"></i>Family Rooms</li>
                            <li><i class="fa fa-wifi"></i>Free Wifi</li>
                            <li><i class="fa fa-car"></i>Free Parking</li>
                            <li><i class="fa fa-heartbeat"></i>Fitness Center</li>
                            <li><i class="fa fa-hotel"></i>Room Service</li>
                            <li><i class="fa fa-coffee"></i>Tea / Coffee Maker</li>
                            <li><i class="fa fa-coffee"></i>Coffee Shop</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Special Packages -->

    <div id="map" class="loc-map">
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages sticky room-details trans-header");
    </script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDBdRx3UKvK7wCC1xx-pYVt_Wihfa41S1M"></script>
    <script type="text/javascript">
        "use strict";
        function initialize() {
            var myLatLng = new google.maps.LatLng(24.820336, 46.648526);
            var mapOptions = {
                zoom: 14,
                center: myLatLng,
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{ featureType: "landscape", stylers: [{ saturation: -100 }, { lightness: 65 }, { visibility: "on" }] }, { featureType: "poi", stylers: [{ saturation: -100 }, { lightness: 51 }, { visibility: "simplified" }] }, { featureType: "road.highway", stylers: [{ saturation: -100 }, { visibility: "simplified" }] }, { featureType: "road.arterial", stylers: [{ saturation: -100 }, { lightness: 30 }, { visibility: "on" }] }, { featureType: "road.local", stylers: [{ saturation: -100 }, { lightness: 40 }, { visibility: "on" }] }, { featureType: "transit", stylers: [{ saturation: -100 }, { visibility: "simplified" }] }, { featureType: "administrative.province", stylers: [{ visibility: "off" }] }, { featureType: "administrative.locality", stylers: [{ visibility: "off" }] }, { featureType: "administrative.neighborhood", stylers: [{ visibility: "on" }] }, { featureType: "water", elementType: "labels", stylers: [{ visibility: "off" }, { lightness: -25 }, { saturation: -100 }] }, { featureType: "water", elementType: "geometry", stylers: [{ hue: "#ffff00" }, { lightness: -25 }, { saturation: -97 }] }],

                // Extra options
                scrollwheel: false,
                mapTypeControl: false,
                panControl: false,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL,
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                }
            }
            var map = new google.maps.Map(document.getElementById('map'), mapOptions);

            var image = 'http://aberhotels.com/assets/img/aber-map-logo.png';
            var beachMarker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

</asp:Content>
