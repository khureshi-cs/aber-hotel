﻿<%@ Page Title="Braira Cordoba Branch Coming Soon" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="braira-cordoba.aspx.cs" Inherits="aber.braira_cordoba" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
		<h1><span>News</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">Home</a></li>
            <li><a href="news.aspx">News</a></li>            
        </ol>
	</div>
	<!-- End of Internal Page Header -->
	
	<!-- Main Container -->
	<div class="main-content container">
		<!-- Page Content -->
		<div class="page-content col-md-12">			
			<!-- Post Container -->
			<div class="post-container">
				<!-- Post boxes -->
				<div class="post-box">
					<img src="assets/img/news/braira-cordoba.jpg" class="post-img" alt="Braira Cordoba"/>
					<h3 class="post-title">Braira Cordoba Branch Coming Soon</h3>
					<div class="post-desc">
						<p>
							The opening of Braira Hotel Cordoba branch, which is characterized by elegant design and modern furniture to suit all tastes, watch our amazing offers and discounts which are unbelievable and enjoy the most beautiful nights in Braira Hotels and Resorts.
						</p>						
					</div>
					<div class="post-meta clearfix">
						<div class="post-date">28 - Oct - 2017</div>
					</div>				
				</div>
			</div>
			<!-- End of Post Container -->
		</div>		
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
