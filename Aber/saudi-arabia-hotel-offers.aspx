﻿<%@ Page Title="Exclusive Latest Offers from Aber Hotels Saudi Arabia" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="saudi-arabia-hotel-offers.aspx.cs" Inherits="aber.saudi_arabia_hotel_offers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
	<!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="assets/img/internal-header.jpg">
		<h1>Latest - <span>Offers</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">Home</a></li>            
            <li class="active">Latest Offers</li>
        </ol>
	</div>
	<!-- End of Internal Page Header -->
	
	<!-- Rooms Container -->
	<div class="room-container container room-list">
		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/standard.jpg" alt="1">
				<a href="#" class="btn btn-default">Book Now</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#">Standard Double <span>Room</span></a></div>
				<div class="desc">				
					<ul class="facilities list-inline">
                        <li><i class="fa fa-check"></i>1 Extra - Large Double Bed</li>
						<li><i class="fa fa-check"></i>Air Conditioning</li>
						<li><i class="fa fa-check"></i>Free Wifi</li>
						<li><i class="fa fa-check"></i>Room Size : 28 m<sup>2</sup></li>
						<li><i class="fa fa-check"></i>Private Bathroom</li>
						<li><i class="fa fa-check"></i>Safety Deposit Box</li>
                        <li><i class="fa fa-check"></i>Hair Dryer</li>
                        <li><i class="fa fa-check"></i>Bathroom</li>
                        <li><i class="fa fa-check"></i>TV</li>
                        <li><i class="fa fa-check"></i>Telephone</li>
                        <li><i class="fa fa-check"></i>Minibar</li>                        
					</ul>
				</div>
				<div class="price">
					<span>SAR 245</span>
					- Per Night
				</div>
			</div>
		</div>
	
		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/deluxe.jpg" alt="Twin Room">
				<a href="#" class="btn btn-default">Book Now</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#">Deluxe Double or  <span>Twin Room</span></a></div>
				<div class="desc">					
					<ul class="facilities list-inline">
                        <li><i class="fa fa-check"></i>2 Queen Beds</li>
						<li><i class="fa fa-check"></i>Air Conditioning</li>
						<li><i class="fa fa-check"></i>Free Wifi</li>
						<li><i class="fa fa-check"></i>Room Size : 38 m<sup>2</sup></li>
						<li><i class="fa fa-check"></i>Private Bathroom</li>
						<li><i class="fa fa-check"></i>Safe</li>
                        <li><i class="fa fa-check"></i>Hair Dryer</li>
                        <li><i class="fa fa-check"></i>Bathroom</li>
                        <li><i class="fa fa-check"></i>TV</li>
                        <li><i class="fa fa-check"></i>Telephone</li>
                        <li><i class="fa fa-check"></i>Minibar</li>                        
					</ul>
				</div>
				<div class="price">
					<span>SAR 295</span>
					- Per Night
				</div>
			</div>
		</div>

		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/super-premium.jpg" alt="two sper premium rooms">
				<a href="#" class="btn btn-default">Book Now</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#">Two <span>Superior Rooms</span></a></div>
				<div class="desc">					
					<ul class="facilities list-inline">
                        <li><i class="fa fa-check"></i>2 Queen Beds</li>
						<li><i class="fa fa-check"></i>Air Conditioning</li>
						<li><i class="fa fa-check"></i>Free Wifi</li>
						<li><i class="fa fa-check"></i>Room Size : 38 m<sup>2</sup></li>
						<li><i class="fa fa-check"></i>Private Bathroom</li>
						<li><i class="fa fa-check"></i>Safe</li>
                        <li><i class="fa fa-check"></i>Hair Dryer</li>
                        <li><i class="fa fa-check"></i>Bathroom</li>
                        <li><i class="fa fa-check"></i>TV</li>
                        <li><i class="fa fa-check"></i>Telephone</li>
                        <li><i class="fa fa-check"></i>Minibar</li>                        
					</ul>
				</div>
				<div class="price">
					<span>SAR 345</span>
					- Per Night
				</div>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky")
    </script>
</asp:Content>
