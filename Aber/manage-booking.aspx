﻿<%@ Page Title="Manage Bookings" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="manage-booking.aspx.cs" Inherits="aber.manage_booking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    
	<!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="assets/img/internal-header.jpg">
		<h1>Manage - <span>Booking</span></h1>		
	</div>
	<!-- End of Internal Page Header -->	
	<!-- Rooms Container -->
	<div class="room-container container room-list">
		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-3">
				<img src="http://via.placeholder.com/290x250" alt="1">				
			</div>
			<div class="details col-xs-9">
				<div class="title"><a href="javascript:;">Aber Diriyah - Riyadh <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></a></div>
				<div class="desc">
					
					<ul class="facilities list-inline">
						<li>Booking number : 123456789</li>
						<li>Check-in<br /><strong>12</strong> July 2017</li>
						<li>Check-out<br /><strong>18</strong> July 2017</li>
						
					</ul>
                    <a href="javascript:;" class="btn btn-default">View Booking</a>
				</div>
				<div class="price">
					<span>240</span>
					SAR 
				</div>
			</div>
		</div>
		<div class="room-box clearfix">
			<div class="img-container col-xs-3">
				<img src="http://via.placeholder.com/290x250" alt="1">				
			</div>
			<div class="details col-xs-9">
				<div class="title"><a href="javascript:;">Aber Diriyah - Riyadh <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></a></div>
				<div class="desc">
					
					<ul class="facilities list-inline">
						<li>Booking number : 123456789</li>
						<li>Check-in<br /><strong>12</strong> July 2017</li>
						<li>Check-out<br /><strong>18</strong> July 2017</li>
						
					</ul>
                    <a href="javascript:;" class="btn btn-default">View Booking</a>
				</div>
				<div class="price">
					<span>240</span>
					SAR 
				</div>
			</div>
		</div>
        <div class="room-box clearfix">
			<div class="img-container col-xs-3">
				<img src="http://via.placeholder.com/290x250" alt="1">				
			</div>
			<div class="details col-xs-9">
				<div class="title"><a href="javascript:;">Aber Diriyah - Riyadh <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></a></div>
				<div class="desc">					
					<ul class="facilities list-inline">
						<li>Booking number : 123456789</li>
						<li>Check-in<br /><strong>12</strong> July 2017</li>
						<li>Check-out<br /><strong>18</strong> July 2017</li>						
					</ul>
                    <a href="javascript:;" class="btn btn-default">View Booking</a>
				</div>
				<div class="price">
					<span>240</span>
					SAR 
				</div>
			</div>
		</div>
        <div class="room-box clearfix">
			<div class="img-container col-xs-3">
				<img src="http://via.placeholder.com/290x250" alt="1">				
			</div>
			<div class="details col-xs-9">
				<div class="title"><a href="javascript:;">Aber Diriyah - Riyadh <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></a></div>
				<div class="desc">
					
					<ul class="facilities list-inline">
						<li>Booking number : 123456789</li>
						<li>Check-in<br /><strong>12</strong> July 2017</li>
						<li>Check-out<br /><strong>18</strong> July 2017</li>
						
					</ul>
                    <a href="javascript:;" class="btn btn-default">View Booking</a>
				</div>
				<div class="price">
					<span>240</span>
					SAR 
				</div>
			</div>
		</div>
	</div>
	<!-- End of Rooms Container -->
	<!-- Pagination -->
	<div class="pagination-box">
        <ul class="list-inline">
            <li class="active"><a href="#"><span>1</span></a></li>
            <li><a href="#"><span>2</span></a></li>
            <li><a href="#"><span>3</span></a></li>
            <li><a href="#"><span>4</span></a></li>
            <li><a href="#"><span>5</span></a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
        </ul>
    </div>
	<!-- End of Pagination -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky")
    </script>
</asp:Content>
