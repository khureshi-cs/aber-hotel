﻿<%@ Page Title="Braira Hotel AL- Wezarat Branch Grand opening" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="braira-al-wizarat.aspx.cs" Inherits="aber.braira_al_wizarat" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
		<h1><span>News</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">Home</a></li>
            <li><a href="news.aspx">News</a></li>            
        </ol>
	</div>
	<!-- End of Internal Page Header -->	
	<!-- Main Container -->
	<div class="main-content container">
		<!-- Page Content -->
		<div class="page-content col-md-12">			
			<!-- Post Container -->
			<div class="post-container">
				<!-- Post boxes -->
				<div class="post-box">
					<img src="assets/img/news/braira-al-wizarat.jpg" class="post-img" alt="Braira Hotel AL- Wezarat"/>
					<h3 class="post-title">Braira Hotel AL- Wezarat Branch Grand Opening</h3>
					<div class="post-desc">
						<p>
							Braira Hotel new branch will be opened soon to public in the ministries district in Riyadh, the hotel is located in a vital and distinctive location in Riyadh in the heart of the city. The hotel is the perfect choice for business and leisure travelers. The hotel is characterized by elegant and contemporary furniture that is very much to the luxury and has a beautiful color and comfortable design for the eyes. Also, the most outstanding of the hotels in Braira is the tranquility and good service with all the integrated services. You will certainly find everything you need for a wonderful stay.
						</p>						
					</div>
					<div class="post-meta clearfix">
						<div class="post-date">28 - Oct - 2017</div>
					</div>				
				</div>
			</div>
			<!-- End of Post Container -->
		</div>		
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
