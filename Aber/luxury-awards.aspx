﻿<%@ Page Title="Luxury Hotel Award" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="luxury-awards.aspx.cs" Inherits="aber.luxury_awards" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
		<h1><span>News</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">Home</a></li>
            <li><a href="news.aspx">News</a></li>            
        </ol>
	</div>
	<!-- End of Internal Page Header -->
	
	<!-- Main Container -->
	<div class="main-content container">
		<!-- Page Content -->
		<div class="page-content col-md-12">			
			<!-- Post Container -->
			<div class="post-container">
				<!-- Post boxes -->
				<div class="post-box">
					<img src="assets/img/news/luxury-awards.jpg" class="post-img" alt="Luxury Awards"/>
					<h3 class="post-title">Luxury Hotel Award</h3>
					<div class="post-desc">
						<p>
							Narcissus Hotel &amp; Residence Riyadh is the Winner of Luxury Hotel World Award in Middle East and Africa for the 4th year.
						</p>						
					</div>
					<div class="post-meta clearfix">
						<div class="post-date">28 - July - 2017</div>
					</div>				
				</div>
			</div>
			<!-- End of Post Container -->
		</div>		
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
