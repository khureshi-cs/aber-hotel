﻿<%@ Page Title="قريباً إفتتاح فندق عابر فرع الصحافة" Language="C#" MasterPageFile="~/ar/Aber.Master" AutoEventWireup="true" CodeBehind="aber-new-branch-opening.aspx.cs" Inherits="aber.ar.aber_new_branch_opening" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
    <div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
        <h1><span>أخبار</span></h1>
        <ol class="breadcrumb">
            <!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">الرئيسية</a></li>
            <li><a href="news.aspx">أخبار</a></li>            
        </ol>
    </div>
    <!-- End of Internal Page Header -->
    <!-- Main Container -->
    <div class="main-content container">
        <!-- Page Content -->
        <div class="page-content col-md-12">
            <!-- Post Container -->
            <div class="post-container">
                <!-- Post boxes -->
                <div class="post-box">                    
                    <img src="assets/img/news/aber-2.jpg" class="post-img" alt="Aber Hotels" />
                    <h3 class="post-title">قريباً إفتتاح فندق عابر فرع الصحافة</h3>
                    <div class="post-desc">
                        <p>
                            قريبا سيتم إفتتاح الفرع الجديد لفندق عابر بحى الصحافة بالرياض , تمتاز فنادق عابر بطابع فخم وبسيط من حيث التصميم والمفهوم وباسعار اقتصادية مع ضمان الجودة العالية والأحترافية في تقديم الخدمات لضيوفنا الكرام . المفهوم الأقتصادي الذي نحرص على تقديمه في هذه المجموعة اصبح حاليا غاية ضرورية للعديد من المسافرين والباحثين عن التغير في نمط الحياة المعتاد . تتميز هذه السلسلة من الفنادق بتقديم خدمات مميزة بأسعار اقتصادية .حيث التصاميم تجمع بين الحداثة والأصالة العربية .
                        </p>
                    </div>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
            </div>
            <!-- End of Post Container -->
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScripts" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
