﻿<%@ Page Title="برج نارسيس الرياض" Language="C#" MasterPageFile="~/ar/Aber.Master" AutoEventWireup="true" CodeBehind="narcisuss-tower-riyadh.aspx.cs" Inherits="aber.ar.narcisuss_tower_riyadh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
    <div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
        <h1><span>أخبار</span></h1>
        <ol class="breadcrumb">
            <!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">الرئيسية</a></li>
            <li><a href="news.aspx">أخبار</a></li>
        </ol>
    </div>
    <!-- End of Internal Page Header -->
    <!-- Main Container -->
    <div class="main-content container">
        <!-- Page Content -->
        <div class="page-content col-md-12">
            <!-- Post Container -->
            <div class="post-container">
                <!-- Post boxes -->
                <div class="post-box">
                    <img src="assets/img/news/narcisuss-tower1.jpg" class="post-img" alt="Narcissus Tower Riyadh" />
                    <h3 class="post-title">برج نارسيس الرياض</h3>
                    <div class="post-desc">
                        <p>
                            يسعدنا أن نعلن قريباًً عن الأفتتاح الرسمى لبرج نارسيس و المجاور لفندق نارسيس الرياض بشارع التحلية, نارسيس أصبح أسم غنى عن التعريف فى عالم الضيافة, فطيلة خمس سنوات منذ الأفتتاح حصد وبكل فخر أربعة جوائز للفخامة العالمية. فندق نارسيس يمتاز بالرونق و الجمال من حيث التصميم الخارجى الذى يوحى بالعراقة و الأصالة , بالأضافة لفخامة المفروشات و التجهيزات الداخلية والتى تبهر أعين كل من يراها, بالأضافة لكثير من الخدمات الأخرى مثل المطاعم و الصالات الرياضية و قاعات الأفراح و المؤتمرات والمسابح و المنتجع الصحى داخل الفندق وغيرها الكثير من المزايا.
                        </p>
                    </div>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
            </div>
            <!-- End of Post Container -->
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScripts" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>

