﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ar/Aber.Master" AutoEventWireup="true" CodeBehind="careers.aspx.cs" Inherits="aber.ar.careers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="assets/img/internal-header.jpg">
		<h1>Careers at<span> Aber Hotels</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">Home</a></li>
            <li class="active">Careers</li>
        </ol>
	</div>
	<!-- End of Internal Page Header -->
	
	<!-- Contact Page Container -->
	<div class="contact-page-container container careers">
        <div class="heading-box">
            <h2>Careers at <span>Aber Hotels</span></h2>
            <!-- Title -->
        </div>
	    <!-- Contact Form -->
		<div class="contact-form-container">
			<div class="how-contact col-md-4">
				<div class="title">Aber Hotels always welcome new talents!</div>
				<div class="desc">
					Aber Hotels is always looking for new talents which can help creating better hospitality.
				</div>
			</div>
			<div class="contact-form-box col-md-8">
				<div class="contact-form clearfix">
					<div class="col-md-6">
						<input type="text" name="name" placeholder="Full Name :">
						<input type="email" name="email" placeholder="Email :">
						<input type="text" name="phone" placeholder="Phone :">
                        <input type="text" name="nationality" placeholder="Nationality :" />
                        <input type="file" name="upload" class="form-control">                        
					</div>
					<div class="col-md-6">
						<textarea name="message" placeholder="Your Message : "></textarea>
					</div>
					<div class="submit-container">
						<input type="submit" value="Submit" class="btn btn-default">
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- End of Contact Page Container -->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScripts" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages sticky trans-header contact")
    </script>
</asp:Content>
