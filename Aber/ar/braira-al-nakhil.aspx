﻿<%@ Page Title="بريرا النخيل ( الرياض )" Language="C#" MasterPageFile="~/ar/Aber.Master" AutoEventWireup="true" CodeBehind="braira-al-nakhil.aspx.cs" Inherits="aber.ar.braira_al_nakhil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
    <div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
        <h1><span>أخبار</span></h1>
        <ol class="breadcrumb">
            <!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">الرئيسية</a></li>
            <li><a href="news.aspx">أخبار</a></li>
        </ol>
    </div>
    <!-- End of Internal Page Header -->
    <!-- Main Container -->
    <div class="main-content container">
        <!-- Page Content -->
        <div class="page-content col-md-12">
            <!-- Post Container -->
            <div class="post-container">
                <!-- Post boxes -->
                <div class="post-box">
                    <img src="assets/img/news/al-nakhil.jpg" class="post-img" alt="Braira Al Nakhil ( Riyadh )" />
                    <h3 class="post-title">بريرا النخيل ( الرياض )</h3>
                    <div class="post-desc">
                        <p>
                            قريباً سيتم إفتتاح فندق بريرا فرع حى النخيل بالرياض , من أكثر ما تمتاز به فنادق بريرا هو الديكور العصرى الذي يميل الى الفخامة والبساطة فى الوقت ذاته, فقد أسست الشركة العديد من الفنادق والمنتجعات فى أنحاء المملكة وجارى التوسع لتشمل عدة مدن رئيسية. يُعد بريرا المكان الأمثل لقضاء عطلة مع العائلة ففيه يسمتع النزيل بالهدوء والرفاهية بالأضافة الى خدمات فندقية متكاملة وبأسعار لا تقارن.
                        </p>
                    </div>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
            </div>
            <!-- End of Post Container -->
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScripts" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
