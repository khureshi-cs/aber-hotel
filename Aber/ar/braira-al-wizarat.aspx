﻿<%@ Page Title="إفتتاح فندق بريرا حى الوزارات" Language="C#" MasterPageFile="~/ar/Aber.Master" AutoEventWireup="true" CodeBehind="braira-al-wizarat.aspx.cs" Inherits="aber.ar.braira_al_wizarat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
    <div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
        <h1><span>أخبار</span></h1>
        <ol class="breadcrumb">
            <!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">الرئيسية</a></li>
            <li><a href="news.aspx">أخبار</a></li>
        </ol>
    </div>
    <!-- End of Internal Page Header -->
    <!-- Main Container -->
    <div class="main-content container">
        <!-- Page Content -->
        <div class="page-content col-md-12">
            <!-- Post Container -->
            <div class="post-container">
                <!-- Post boxes -->
                <div class="post-box">
                    <img src="assets/img/news/braira-al-wizarat.jpg" class="post-img" alt="Braira Hotel AL- Wezarat Branch Grand opening" />
                    <h3 class="post-title">إفتتاح فندق بريرا حى الوزارات</h3>
                    <div class="post-desc">
                        <p>
                            قريبا سيتم إفتتاح فندق بريرا حى الوزارات بمدينة الرياض, فالفندق يقع فى موقع حيوي ومميز بالرياض فى قلب المدينة. يُعد الفندق هو الخيار الأمثل لرجال الأعمال و الباحثيين عن مكان للإقامة بجوار المؤسسات والهئيات. يمتاز الفندق بآثاث عصرى و أنيق يميل الى حد كبير الى الفخامة و يتمتع بتصاميم ذات ألوان خلابة و مريحة للأعين , أيضا من أكثر ما يميز فنادق بريرا هو الهدوء وحسن الخدمة الفندقية مع جميع الخدمات المتكاملة فهنالك بالتأكيد ستجد كل ما تحتاجه لقضاء إقامة أكثر من رائعة وبأسعار ممتازة.
                        </p>
                    </div>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
            </div>
            <!-- End of Post Container -->
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScripts" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
