﻿<%@ Page Title="جائزة الفخامة الفندقية العالمية" Language="C#" MasterPageFile="~/ar/Aber.Master" AutoEventWireup="true" CodeBehind="luxury-awards.aspx.cs" Inherits="aber.ar.luxury_awards" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
    <div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
        <h1><span>أخبار</span></h1>
        <ol class="breadcrumb">
            <!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">الرئيسية</a></li>
            <li><a href="news.aspx">أخبار</a></li>            
        </ol>
    </div>
    <!-- End of Internal Page Header -->
    <!-- Main Container -->
    <div class="main-content container">
        <!-- Page Content -->
        <div class="page-content col-md-12">
            <!-- Post Container -->
            <div class="post-container">
                <!-- Post boxes -->
                <div class="post-box">                    
                    <img src="assets/img/blog/aber-news.jpg" alt="Aber hotel news" class="post-img">                    
                    <h3 class="post-title">جائزة الفخامة الفندقية العالمية</h3>
                    <div class="post-desc">
                        <p>
                            للسنة الرابعة على التوالى يحصد فندق نارسيس الرياض جائزة الفخامة الفندقية العالمية لسنة ٢٠١٧ لكونه أفخم فندق من حيث التصميم المعمارى و الديكور الداخلى فى الشرق الأوسط و إفريقيا.
                        </p>
                    </div>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
            </div>
            <!-- End of Post Container -->
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScripts" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
