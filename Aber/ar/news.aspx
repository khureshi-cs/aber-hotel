﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ar/Aber.Master" AutoEventWireup="true" CodeBehind="news.aspx.cs" Inherits="aber.ar._new" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="internal-page-title about-page" data-parallax="scroll" data-image-src="../assets/img/internal-header.jpg">
        <h1>آخر <span>الأخبار والإعلان</span></h1>
        <ol class="breadcrumb">
            <!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">الرئيسية</a></li>
            <li class="active">الأخبار</li>
        </ol>
    </div>
    <!-- End of Internal Page Header -->

    <!-- Main Container -->
    <div class="main-content container news">
        <!-- Page Content -->
        <div class="page-content col-md-12">
            <!-- Post Container -->
            <div class="post-container">
                <!-- Post boxes -->
                <div class="post-box">
                    <a href="luxury-awards.aspx">
                        <img src="assets/img/news/luxury-awards.jpg" class="post-img" alt="Luxury Awards" />
                    </a>
                    <a href="luxury-awards.aspx" class="post-title">جائزة الفخامة الفندقية العالمية</a>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
                <!-- Post boxes -->
                <div class="post-box">
                    <a href="braira-cordoba.aspx">
                        <img src="assets/img/news/braira-cordoba.jpg" class="post-img" alt="Braira Cordoba" />
                    </a>
                    <a href="braira-cordoba.aspx" class="post-title">قريباً إفتتاح فندق بريرا فرع قرطبة بالرياض</a>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
                <!-- Post boxes -->
                <div class="post-box">
                    <a href="aber-new-branch-opening.aspx">
                        <img src="assets/img/news/aber-2.jpg" class="post-img" alt="Aber Hotels" />
                    </a>
                    <a href="aber-new-branch-opening.aspx" class="post-title">قريباً إفتتاح فندق عابر فرع الصحافة</a>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
                <!-- Post boxes -->
                <div class="post-box">
                    <a href="narcisuss-tower-riyadh.aspx">
                        <img src="assets/img/news/narcisuss-tower1.jpg" class="post-img" alt="Narcissus Tower Riyadh" />
                    </a>
                    <a href="narcisuss-tower-riyadh.aspx" class="post-title">برج نارسيس الرياض</a>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
                <!-- Post boxes -->
                <div class="post-box">
                    <a href="braira-al-nakhil.aspx">
                        <img src="assets/img/news/al-nakhil.jpg" class="post-img" alt="Braira Al Nakhil ( Riyadh )" />
                    </a>
                    <a href="braira-al-nakhil.aspx" class="post-title">بريرا النخيل ( الرياض )</a>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
                <!-- Post boxes -->
                <div class="post-box">
                    <a href="braira-al-wizarat.aspx">
                        <img src="assets/img/news/braira-al-wizarat.jpg" class="post-img" alt="Braira Hotel AL- Wezarat Branch Grand opening" />
                    </a>
                    <a href="braira-al-wizarat.aspx" class="post-title">إفتتاح فندق بريرا حى الوزارات</a>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
                <!-- Post boxes -->
                <div class="post-box">
                    <a href="braira-villas.aspx">
                        <img src="assets/img/news/braira-hitin.jpg" class="post-img" alt="Braira Villas Grand Opening" />
                    </a>
                    <a href="braira-villas.aspx" class="post-title">إفتتاح منتجع فيلات بريرا</a>
                    <div class="post-meta clearfix">
                        <div class="post-date">28 - Oct - 2017</div>
                    </div>
                </div>
            </div>
            <!-- End of Post Container -->
            <!-- Pagination -->
            <div class="pagination-box">
                <ul class="list-inline">
                    <li class="active"><a href="#"><span>1</span></a></li>
                    <li><a href="#"><span>2</span></a></li>
                    <li><a href="#"><span>3</span></a></li>
                    <li><a href="#"><span>4</span></a></li>
                    <li><a href="#"><span>5</span></a></li>
                    <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                </ul>
            </div>
            <!-- End of Pagination -->
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScripts" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
