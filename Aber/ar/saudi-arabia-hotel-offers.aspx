﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ar/Aber.Master" AutoEventWireup="true" CodeBehind="saudi-arabia-hotel-offers.aspx.cs" Inherits="aber.ar.saudi_arabia_hotel_offers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
	<!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="assets/img/internal-header.jpg">
		<h1>اخر  - <span>العروض</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">الرئيسية</a></li>    
            <li class="active">اخر العروض</li>
        </ol>
	</div>
	<!-- End of Internal Page Header -->
	
	<!-- Rooms Container -->
	<div class="room-container container room-list">
		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/standard.jpg" alt="1">
				<a href="#" class="btn btn-default">احجز الأن</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#">غرفة قياسية مزدوجة</a></div>
				<div class="desc">				
					<ul class="facilities list-inline">
                        <li><i class="fa fa-check"></i>سرير مزدوج كبير جدًا</li>
						<li><i class="fa fa-check"></i>تكييف</li>
						<li><i class="fa fa-check"></i>واي فاي استثنائي مجاناً</li>
						<li><i class="fa fa-check"></i>مساحة الغرفة : 28 م<sup>2</sup></li>
						<li><i class="fa fa-check"></i>حمام خاص</li>
						<li><i class="fa fa-check"></i>صندوق أمانات</li>
                        <li><i class="fa fa-check"></i>مجفف شعر</li>
                        <li><i class="fa fa-check"></i>حمام</li>
                        <li><i class="fa fa-check"></i>هاتف</li>
                        <li><i class="fa fa-check"></i>تليفزيون</li>
                        <li><i class="fa fa-check"></i>ميني بار</li>                        
					</ul>
				</div>
				<div class="price">
					<span>ر.س.‏ 245</span>
					- ليلة واحدة
				</div>
			</div>
		</div>
	
		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/deluxe.jpg" alt="Twin Room">
				<a href="#" class="btn btn-default">احجز الأن</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#">غرفة ديلوكس مزدوجة أو توأم</a></div>
				<div class="desc">					
					<ul class="facilities list-inline">
                        <li><i class="fa fa-check"></i>سرير مزدوج كبير جدًا</li>
						<li><i class="fa fa-check"></i>تكييف</li>
						<li><i class="fa fa-check"></i>واي فاي استثنائي مجاناً</li>
						<li><i class="fa fa-check"></i>مساحة الغرفة : 38 م<sup>2</sup></li>
						<li><i class="fa fa-check"></i>حمام خاص</li>
						<li><i class="fa fa-check"></i>صندوق أمانات</li>
                        <li><i class="fa fa-check"></i>مجفف شعر</li>
                        <li><i class="fa fa-check"></i>حمام</li>
                        <li><i class="fa fa-check"></i>هاتف</li>
                        <li><i class="fa fa-check"></i>تليفزيون</li>
                        <li><i class="fa fa-check"></i>ميني بار</li>                      
					</ul>
				</div>
				<div class="price">
					<span>ر.س.‏ 295</span>
					- ليلة واحدة
				</div>
			</div>
		</div>

		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/super-premium.jpg" alt="two sper premium rooms">
				<a href="#" class="btn btn-default">احجز الأن</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#">Two <span>Superior Rooms</span></a></div>
				<div class="desc">					
					<ul class="facilities list-inline">
                        <li><i class="fa fa-check"></i>سرير مزدوج كبير جدًا</li>
						<li><i class="fa fa-check"></i>تكييف</li>
						<li><i class="fa fa-check"></i>واي فاي استثنائي مجاناً</li>
						<li><i class="fa fa-check"></i>مساحة الغرفة : 38 م<sup>2</sup></li>
						<li><i class="fa fa-check"></i>حمام خاص</li>
						<li><i class="fa fa-check"></i>صندوق أمانات</li>
                        <li><i class="fa fa-check"></i>مجفف شعر</li>
                        <li><i class="fa fa-check"></i>حمام</li>
                        <li><i class="fa fa-check"></i>هاتف</li>
                        <li><i class="fa fa-check"></i>تليفزيون</li>
                        <li><i class="fa fa-check"></i>ميني بار</li>                       
					</ul>
				</div>
				<div class="price">
					<span>ر.س.‏ 345</span>
					- ليلة واحدة
				</div>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScripts" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky")
    </script>
</asp:Content>
