﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ar/Aber.Master" AutoEventWireup="true" CodeBehind="about-us.aspx.cs" Inherits="aber.ar.about_us" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
    <div class="internal-page-title about-page" data-parallax="scroll" data-image-src="assets/img/internal-header.jpg">
        <h1>نبذة عن    <span>فندق عابر</span></h1>
        <ol class="breadcrumb">
            <!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">الرئيسية</a></li>
            <li class="active">نبذة عن</li>
        </ol>
    </div>
    <!-- End of Internal Page Header -->

    <!-- Welcome -->
    <div id="welcome" class="container">
        <!-- Heading box -->
        <div class="heading-box">
            <h2>مرحبا بكم في  <span>فندق عابر</span></h2>
            <!-- Title -->
        </div>
        <!-- Inner section -->
        <div class="inner-content">
            <div class="img-frame">
                <img src="assets/img/aber-about-us.jpg" alt="Welcome to Aber">
            </div>
            <div class="desc">
                <p>
                    انشأت شركة " بودل " للفنادق والمنتجعات سلسلة " فنادق عابر " و أطلقت علامتها التجارية في عام 2016 
لتلبية احتياجات العديد من شرائح المجتمع التي نتشرف بمخاطبتها من خلال مجموعة فنادق حديثة من حيث 
التصميم والمفهوم وباسعار اقتصادية مع ضمان الجودة العالية والأحترافية 
في تقديم الخدمات لضيوفنا الكرام . المفهوم الأقتصادي الذي نحرص على تقديمه في هذه المجموعة 

                </p>
                <p>
                    اصبح حاليا غاية ضرورية للعديد من المسافرين والباحثين عن التغير في نمط الحياة المعتاد .
تتميز هذه السلسلة من الفنادق بتقديم خدمات مميزة بأسعار اقتصادية .حيث التصاميم تجمع بين الحداثة والأصالة العربية .
تم إنشاء عدد 6 فنادق من سلسلة فنادق عابر في كل من مدينة (الرياض - القصيم - المنطقة الجنوبية - والمنطقة الشرقية )
وخطتنا الأستراتيجية تتضمن ان تنتشر هذه المجموعة في كل انحاء المملكة .

                </p>
            </div>
        </div>
    </div>
    <!-- End of Welcome -->

    <!-- Our Services -->
    <div id="our-services">
        <!-- Heading box -->
        <div class="heading-box">
            <h2><span>خدماتنا</span></h2>
            <!-- Title -->
        </div>

        <!-- Service Slider -->
        <div id="services-box" class="owl-carousel owl-theme owl-rtl">
            <div class="item">
                <img src="assets/img/services/swimming-pools.jpg" alt="Swimming Pools">
                <div class="title">حوض سباحة</div>                
            </div>
            <div class="item">
                <img src="assets/img/services/high-speed-internet.jpg" alt="High Speed Internet">
                <div class="title">إنترنت عالي السرعة</div>
                
            </div>
            <div class="item">
                <img src="assets/img/services/meeting-rooms.jpg" alt="meeting rooms">
                <div class="title">غرف الإجتماعات</div>                
            </div>
            <div class="item">
                <img src="assets/img/services/relaxing-atmosphere.jpg" alt="Relaxing Atmosphere">
                <div class="title">جو من الاسترخاء</div>                
            </div>            
        </div>

    </div>
    <!-- End of Our Services -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScripts" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky")
    </script>
</asp:Content>                      