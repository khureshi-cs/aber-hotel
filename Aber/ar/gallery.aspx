﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ar/Aber.Master" AutoEventWireup="true" CodeBehind="gallery.aspx.cs" Inherits="aber.ar.gallery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
    <div class="internal-page-title about-page" data-parallax="scroll" data-image-src="assets/img/internal-header.jpg">
		<h1>معرض الميديا - <span>فندق عابر</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">الرئيسية</a></li>
            <li class="active">معرض الميديا</li>
        </ol>
	</div>
	<!-- End of Internal Page Header -->
	
	<!-- Gallery Container -->
	<div class="gallery-container container gallery-grid">
		<!-- Gallery Container -->
		<div class="gallery-container">
			<div class="sort-section">
				<div class="sort-section-container">
					<div class="sort-handle">Filters</div>
					<ul class="list-inline">
						<li><a href="#" data-filter="*" class="active">الكل</a></li>												
						<li><a href="#" data-filter=".rooms">الاجنحة و الغرفة</a></li>
						<li><a href="#" data-filter=".lobby">لوبي الفندق</a></li>
                        <li><a href="#" data-filter=".pool">المسبح</a></li>
                        <li><a href="#" data-filter=".relaxation">ملاحقات اخرى</a></li>
					</ul>
				</div>
			</div>
			<ul class="image-main-box clearfix">
				<li class="item col-xs-6 col-md-4 rooms">
					<figure>
						<img src="assets/img/gallery/gallery-1.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-1.jpg" class="more-details" >تكبير</a>
						
					</figure>
				</li>							
				<li class="item col-xs-6 col-md-4 lobby">
					<figure>
						<img src="assets/img/gallery/gallery-4.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-4.jpg" class="more-details" >تكبير</a>
						
					</figure>
				</li>
                <li class="item col-xs-6 col-md-4 rooms">
					<figure>
						<img src="assets/img/gallery/gallery-3.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-3.jpg" class="more-details" >تكبير</a>
						
					</figure>
				</li>
				<li class="item col-xs-6 col-md-4 lobby">
					<figure>
						<img src="assets/img/gallery/gallery-5.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-5.jpg" class="more-details" >تكبير</a>
						
					</figure>
				</li>
                <li class="item col-xs-6 col-md-4 rooms">
					<figure>
						<img src="assets/img/gallery/gallery-2.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-2.jpg" class="more-details">تكبير</a>
						
					</figure>
				</li>	
				<li class="item col-xs-6 col-md-4 lobby">
					<figure>
						<img src="assets/img/gallery/gallery-6.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-6.jpg" class="more-details">تكبير</a>
						
					</figure>
				</li>
				<li class="item col-xs-6 col-md-4 lobby">
					<figure>
						<img src="assets/img/gallery/gallery-7.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-7.jpg" class="more-details" >تكبير</a>
						
					</figure>
				</li>
				<li class="item col-xs-6 col-md-4 pool">
					<figure>
						<img src="assets/img/gallery/gallery-8.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-8.jpg" class="more-details">تكبير</a>
						
					</figure>
				</li>
				<li class="item col-xs-6 col-md-4 pool">
					<figure>
						<img src="assets/img/gallery/gallery-9.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-9.jpg" class="more-details">تكبير</a>
						
					</figure>
				</li>
                <li class="item col-xs-6 col-md-4 relaxation">
					<figure>
						<img src="assets/img/gallery/gallery-10.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-10.jpg" class="more-details">تكبير</a>
						
					</figure>
				</li>
                <li class="item col-xs-6 col-md-4 rooms">
					<figure>
						<img src="assets/img/gallery/gallery-11.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-11.jpg" class="more-details">تكبير</a>						
					</figure>
				</li>
                <li class="item col-xs-6 col-md-4 relaxation">
					<figure>
						<img src="assets/img/gallery/gallery-12.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-12.jpg" class="more-details">تكبير</a>						
					</figure>
				</li>
                <li class="item col-xs-6 col-md-4 rooms">
					<figure>
						<img src="assets/img/gallery/gallery-13.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-13.jpg" class="more-details" >تكبير</a>						
					</figure>
				</li>
                <li class="item col-xs-6 col-md-4 relaxation">
					<figure>
						<img src="assets/img/gallery/gallery-14.jpg" alt="11"/>
						<a href="assets/img/gallery/gallery-14.jpg" class="more-details" >تكبير</a>						
					</figure>
				</li>
			</ul>
		</div>
	</div>
	<!-- End of Gallery Container -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterScripts" runat="server">
    <script type="text/javascript">
        $("body").addClass("internal-pages trans-header sticky");
    </script>
</asp:Content>
