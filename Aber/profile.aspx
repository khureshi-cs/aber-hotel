﻿<%@ Page Title="Profile Settings | Personal Information" Language="C#" MasterPageFile="~/Aber.Master" AutoEventWireup="true" CodeBehind="profile.aspx.cs" Inherits="aber.profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Internal Page Header -->
	<div class="internal-page-title about-page" data-parallax="scroll" data-image-src="assets/img/internal-header.jpg">
		<h1>Latest - <span>Offers</span></h1>
		<ol class="breadcrumb"><!-- Internal Page Breadcrumb -->
            <li><a href="default.aspx">Home</a></li>
            <li>Rooms</li>
            <li class="active">Rooms - List View</li>
        </ol>
	</div>
	<!-- End of Internal Page Header -->
	
	<!-- Rooms Container -->
	<div class="room-container container room-list">
		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/1.jpg" alt="1">
				<a href="#" class="btn btn-default">More Details</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#">Single <span>Room</span></a></div>
				<div class="desc">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, pariatur, officiis. Cupiditate autem, accusantium, saepe quasi eaque labore deleniti alias?
					<ul class="facilities list-inline">
						<li><i class="fa fa-check"></i>Breakfast Included</li>
						<li><i class="fa fa-check"></i>Free Wifi</li>
						<li><i class="fa fa-check"></i>Room Size : 60 sqm</li>
						<li><i class="fa fa-check"></i>Max 2 people</li>
						<li><i class="fa fa-check"></i>Sea View</li>
					</ul>
				</div>
				<div class="price">
					<span>$240</span>
					- Per Night
				</div>
			</div>
		</div>

		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/2.jpg" alt="2">
				<a href="#" class="btn btn-default">More Details</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#">Double <span>Room</span></a></div>
				<div class="desc">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, pariatur, officiis. Cupiditate autem, accusantium, saepe quasi eaque labore deleniti alias?
					<ul class="facilities list-inline">
						<li><i class="fa fa-check"></i>Breakfast Included</li>
						<li><i class="fa fa-check"></i>Free Wifi</li>
						<li><i class="fa fa-check"></i>Room Size : 60 sqm</li>
						<li><i class="fa fa-check"></i>Max 2 people</li>
						<li><i class="fa fa-check"></i>Sea View</li>
					</ul>
				</div>
				<div class="price">
					<span>$350</span>
					- Per Night
				</div>
			</div>
		</div>

		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/3.jpg" alt="">
				<a href="#" class="btn btn-default">More Details</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#">Deluxe <span>One-bedroom</span> Suite</a></div>
				<div class="desc">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, pariatur, officiis. Cupiditate autem, accusantium, saepe quasi eaque labore deleniti alias?
					<ul class="facilities list-inline">
						<li><i class="fa fa-check"></i>Breakfast Included</li>
						<li><i class="fa fa-check"></i>Free Wifi</li>
						<li><i class="fa fa-check"></i>Room Size : 60 sqm</li>
						<li><i class="fa fa-check"></i>Max 2 people</li>
						<li><i class="fa fa-check"></i>Sea View</li>
					</ul>
				</div>
				<div class="price">
					<span>$440</span>
					- Per Night
				</div>
			</div>
		</div>

		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/4.jpg" alt="">
				<a href="#" class="btn btn-default">More Details</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#">Deluxe <span>Two-bedroom</span> Suite</a></div>
				<div class="desc">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, pariatur, officiis. Cupiditate autem, accusantium, saepe quasi eaque labore deleniti alias?
					<ul class="facilities list-inline">
						<li><i class="fa fa-check"></i>Breakfast Included</li>
						<li><i class="fa fa-check"></i>Free Wifi</li>
						<li><i class="fa fa-check"></i>Room Size : 60 sqm</li>
						<li><i class="fa fa-check"></i>Max 2 people</li>
						<li><i class="fa fa-check"></i>Sea View</li>
					</ul>
				</div>
				<div class="price">
					<span>$480</span>
					- Per Night
				</div>
			</div>
		</div>

		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/5.jpg" alt="">
				<a href="#" class="btn btn-default">More Details</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#"><span>Royal</span> Suit</a></div>
				<div class="desc">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, pariatur, officiis. Cupiditate autem, accusantium, saepe quasi eaque labore deleniti alias?
					<ul class="facilities list-inline">
						<li><i class="fa fa-check"></i>Breakfast Included</li>
						<li><i class="fa fa-check"></i>Free Wifi</li>
						<li><i class="fa fa-check"></i>Room Size : 60 sqm</li>
						<li><i class="fa fa-check"></i>Max 2 people</li>
						<li><i class="fa fa-check"></i>Sea View</li>
					</ul>
				</div>
				<div class="price">
					<span>$530</span>
					- Per Night
				</div>
			</div>
		</div>

		<!-- Room Boxes -->
		<div class="room-box clearfix">
			<div class="img-container col-xs-6">
				<img src="assets/img/rooms/grid/6.jpg" alt="">
				<a href="#" class="btn btn-default">More Details</a>
			</div>
			<div class="details col-xs-6">
				<div class="title"><a href="#"><span>King</span> Suit</a></div>
				<div class="desc">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, pariatur, officiis. Cupiditate autem, accusantium, saepe quasi eaque labore deleniti alias?
					<ul class="facilities list-inline">
						<li><i class="fa fa-check"></i>Breakfast Included</li>
						<li><i class="fa fa-check"></i>Free Wifi</li>
						<li><i class="fa fa-check"></i>Room Size : 60 sqm</li>
						<li><i class="fa fa-check"></i>Max 2 people</li>
						<li><i class="fa fa-check"></i>Sea View</li>
					</ul>
				</div>
				<div class="price">
					<span>$620</span>
					- Per Night
				</div>
			</div>
		</div>
	</div>
	<!-- End of Rooms Container -->

	<!-- Pagination -->
	<div class="pagination-box">
        <ul class="list-inline">
            <li class="active"><a href="#"><span>1</span></a></li>
            <li><a href="#"><span>2</span></a></li>
            <li><a href="#"><span>3</span></a></li>
            <li><a href="#"><span>4</span></a></li>
            <li><a href="#"><span>5</span></a></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
        </ul>
    </div>
	<!-- End of Pagination -->
	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" runat="server">
</asp:Content>
